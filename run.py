from argparse import ArgumentParser
import os, subprocess, re

# Constants
EXTENSIONS  = ['.mkv']
METADATA_OUT = 'info.txt'

# Get command line args
parser = ArgumentParser()
parser.add_argument(
	'input_dir',
	help = 'specify the input directory for the videos',
	type = str,
)

args = parser.parse_args()

# Check if the input directory does not exist
if not os.path.isdir(args.input_dir):
	print('The provided input directory was invalid.')
	exit(1)

def strip_whitespace(string):
	return re.sub('\s+', '', string).strip()

def _int(s):
	index = 0

	for c in s:
		if c.isdigit():
			index += 1
		else:
			break

	return int(s[:index])

def metadata_to_dict(metadata):
	d = {}
	key = 'error'

	for line in metadata.split('\n'):
		line = re.sub('\s+', ' ', line.lower()).strip()
		tokens = line.split(' : ')

		if not line or line.isspace():
			continue

		if len(tokens) == 1:
			key = line
			d[key] = {}
			continue

		d[key][tokens[0]] = tokens[1]

	return d

def get_video_metadata(filepath):
	result = subprocess.run(['MediaInfo', filepath], stdout=subprocess.PIPE)
	output = result.stdout.decode('utf-8')
	return metadata_to_dict(output)

def increment_key(d, key):
	if key in d:
		d[key] += 1
	else:
		d[key] = 1

def print_dict_sorted_keys(d, name):
	print(name + ': [', end='')
	for k, v in reversed(sorted(d.items())):
		print(k, ':', str(v) + ', ', end='')
	print(']')

widths, heights, ratios = {}, {}, {}
count = 0

for filename in os.listdir(args.input_dir):
	filepath = os.path.join(args.input_dir, filename)
	metadata = get_video_metadata(filepath)

	increment_key(widths, _int(strip_whitespace(metadata['video']['width'])))
	increment_key(heights, _int(strip_whitespace(metadata['video']['height'])))
	increment_key(ratios, metadata['video']['display aspect ratio'])

print_dict_sorted_keys(widths, 'widths')
print_dict_sorted_keys(heights, 'heights')
print_dict_sorted_keys(ratios, 'ratios')